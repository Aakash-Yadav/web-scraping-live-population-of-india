'Import'
from requests import get 
from bs4 import BeautifulSoup
from datetime import datetime
from pandas import read_html,DataFrame
import csv 


def get_html_doc(url:str):
    return get(url).text 

def bs4_data(url):
    return BeautifulSoup(get_html_doc(url),'lxml')

'Main_soup'

soup = bs4_data('https://www.livepopulation.com/country/india.html')

def make_csv(data,name):
    data= read_html(str(data))
    (data[0].to_csv(f'{name}.csv',index=False))
    return True 


def population_of_india_2021():
    'Today Date'
    all_data=(soup.find('div',{'class':'page-content'}))
    population_dict ={
        'Total_population':all_data.find('b',{'class':'current-population'}).text,
        'Total_births_this_year':all_data.find('b',{'class':'birth-year'}).text, 
        'Total_birth_today':all_data.find('b',{'class':'birth-day'}).text, 
        'Death_this_year':all_data.find('b',{'class':'death-year'}).text, 
        'Death_day_today':all_data.find('b',{'class':'death-day'}).text}

    with open('Today.txt','w') as f:

        f.write(f""" 
Today Population = {population_dict['Total_population']}\n
Total births this year = {population_dict['Total_births_this_year']}\n
Total Death this year = {population_dict['Death_this_year']}\n
Death_day_today = {population_dict['Death_day_today']}
                """)

    return population_dict

def india_population_by_religions():
    from pandas import read_html
    table_religions = soup.find('table',{'class':'table table-striped'})
    make_csv(name='Population_by_Religions',data=table_religions)
    return True

def india_population_by_ethnic_groups():
    'table table-striped'
    table_ethnic_groups = soup.find('table',{'class':'table table-striped'})
    make_csv(name='Population_Ethnic_groups',data=table_ethnic_groups)
    return True 

def india_population_by_age_group():
    'table table-striped'
    table_population_by_Age_group =  read_html('https://www.livepopulation.com/country/india.html')
    data = table_population_by_Age_group[2]
    # make_csv(name='population_by_age_group',data=data)
    (data.to_csv('population_by_age_group.csv',index=False))
    return True 

def india_Population_history():
    'table'
    table_india_Population_history = read_html('https://www.livepopulation.com/country/india.html')
    data = table_india_Population_history[3]
    (data.to_csv('india_Population_history.csv',index=False))
    return True 


'list-unstyled popular-home'
def Future_data():
    import csv
    csv_file= open('future.csv','w')
    csv_writer = csv.writer(csv_file)
    'append Row'
    csv_writer.writerow(['Year','Population',])
    'called fun (bs4 data )'
    Future_data_india = soup.find_all('ul',{'class':'list-unstyled popular-home'})
    for data in Future_data_india:
        row = data.find_all('li')
        for value in row:
            p=value.find('p')
            a = value.find('a')
            csv_writer.writerow([a.text,p.text])
    csv_file.close()

if __name__ == '__main__':
    print(population_of_india_2021())
    print(india_population_by_religions())
    print(india_population_by_ethnic_groups())
    print(india_population_by_age_group())
    print(india_Population_history())
    print(Future_data())






